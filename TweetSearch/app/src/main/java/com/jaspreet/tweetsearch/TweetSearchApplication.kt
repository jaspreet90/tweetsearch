package com.jaspreet.tweetsearch

import android.app.Activity
import android.app.Application
import com.jaspreet.tweetsearch.di.component.ApplicationComponent
import com.jaspreet.tweetsearch.di.component.DaggerApplicationComponent
import com.jaspreet.tweetsearch.di.module.ApplicationModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject

class TweetSearchApplication : Application(),   HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)

        setupDefaultRxJavaErrorHandler()

    }

    private fun setupDefaultRxJavaErrorHandler() {
        RxJavaPlugins.setErrorHandler { throwable ->
            throwable.printStackTrace()
        }
    }


    override fun activityInjector() = activityInjector



}