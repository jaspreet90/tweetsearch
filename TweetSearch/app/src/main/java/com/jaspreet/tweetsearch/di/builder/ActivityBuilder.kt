package com.jaspreet.tweetsearch.di.builder

import com.jaspreet.tweetsearch.ui.tweetsearch.TweetSearchActivity
import com.jaspreet.tweetsearch.ui.tweetsearch.TweetSearchModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(TweetSearchModule::class)])
    abstract fun bindTweeetActivity(): TweetSearchActivity
}