package com.jaspreet.tweetsearch.di.component


import com.jaspreet.tweetsearch.TweetSearchApplication
import com.jaspreet.tweetsearch.di.builder.ActivityBuilder
import com.jaspreet.tweetsearch.di.module.ApplicationModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (ApplicationModule::class), (ActivityBuilder::class) ])
interface ApplicationComponent {

    fun inject(app: TweetSearchApplication)

}