package com.jaspreet.tweetsearch.di.module

import android.app.Application
import android.content.Context
import com.jaspreet.tweetsearch.repo.pref.AppPrefrences
import com.jaspreet.tweetsearch.repo.pref.AppPrefrencesImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [(NetworkModule::class)])
class ApplicationModule (val app: Application){

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context =app


    @Provides
    @Singleton
    internal fun providePreferencesHelper(appPrefrencesImpl: AppPrefrencesImpl): AppPrefrences = appPrefrencesImpl

}