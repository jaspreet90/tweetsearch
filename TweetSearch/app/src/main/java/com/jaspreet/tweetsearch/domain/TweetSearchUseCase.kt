package com.jaspreet.tweetsearch.domain

import android.text.TextUtils
import android.util.Base64
import android.util.Log
import com.jaspreet.tweetsearch.repo.network.ApiService
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.TweetsResponse
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.oauth.OAuthResponse
import com.jaspreet.tweetsearch.repo.pref.AppPrefrences
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.io.UnsupportedEncodingException
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TweetSearchUseCase @Inject constructor ( private val mTwitterApi: ApiService ,
                                               private val mSharedPreferences: AppPrefrences

) {

    val mConsumerKey: String = "Xkg8shf0G5Y893Q3wVwY0xaic"
    val mConsumerSecret: String = "1ujUA65iZZDtHV5XYMe7zoI9gDFB6S1CabOujpDYay9orwyDMc"

    private val authorizationHeader: String?
        get() {
            try {
                val consumerKeyAndSecret = "$mConsumerKey:$mConsumerSecret"
                val data = consumerKeyAndSecret.toByteArray(charset("UTF-8"))
                val base64 = Base64.encodeToString(data, Base64.NO_WRAP)
                Log.e("==auth==", base64)
                return "Basic $base64"
            } catch (e: UnsupportedEncodingException) {
                return null
            }
        }

    private val accessToken: String?
        get() {
            val accessToken = mSharedPreferences.accessToken
            if (TextUtils.isEmpty(accessToken)) {
                return null
            }
            return "Bearer " + accessToken!!
        }

    fun searchTweets(query: String)  : Observable<TweetsResponse> {
        val accessToken = accessToken
        return if (TextUtils.isEmpty(accessToken)) {
            requestAccessTokenAndGetTweetList(query)
        } else {
            getTweetList(query)
        }
    }


    private fun requestAccessTokenAndGetTweetList(
        query: String
    )  : Observable<TweetsResponse> {

        return   reqAccessToken()
            .flatMap {
                getTweetList(query)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

    fun reqAccessToken() : Observable<OAuthResponse> {

        return mTwitterApi.getAccessTokn(authorizationHeader!!, "client_credentials")
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .doOnNext {
                saveAccessToken(it.accessToken)
            }


    }

    fun getTweetList(query: String) : Observable<TweetsResponse> {
        return  mTwitterApi.searchTweeet(accessToken!!, query).observeOn(Schedulers.io()).observeOn(Schedulers.io())
    }

    private fun saveAccessToken(accessToken: String) {
        mSharedPreferences.accessToken =accessToken
    }
}
