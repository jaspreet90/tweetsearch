package com.jaspreet.tweetsearch.repo.network

import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.TweetsResponse
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.oauth.OAuthResponse
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiRepositoryImpl @Inject
constructor( private val apiService: ApiService ) : ApiRepository {
    override fun getAccessTokn(authorization: String, grantType: String): Observable<OAuthResponse> = apiService.getAccessTokn(authorization,grantType)


    override fun searchTweeet(authorization: String, query: String): Observable<TweetsResponse> = apiService.searchTweeet(authorization,query)

}
