package com.jaspreet.tweetsearch.repo.network

 import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.TweetsResponse
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.oauth.OAuthResponse
 import io.reactivex.Observable
 import io.reactivex.Single
import retrofit2.http.*

interface ApiService {


    companion object {
        const val HEADER_PARAM_SEPARATOR = ":"


    }

    @POST("oauth2/token")
    @FormUrlEncoded
    fun getAccessTokn(  @Header("Authorization") authorization: String, @Field("grant_type") grantType: String ): Observable<OAuthResponse>

    @GET("1.1/search/tweets.json")
    fun searchTweeet(@Header("Authorization") authorization :String   ,@Query("q")  query : String): Observable<TweetsResponse>



}