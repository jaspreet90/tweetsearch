
package com.jaspreet.tweetsearch.repo.network.model.tweetsearch

import com.google.gson.annotations.SerializedName

data class TweetsResponse (
    @SerializedName("statuses")
    var tweets: List<Twit>? = null
)
