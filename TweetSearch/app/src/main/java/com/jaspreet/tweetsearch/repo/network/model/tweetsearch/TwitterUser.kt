
package com.jaspreet.tweetsearch.repo.network.model.tweetsearch

import com.google.gson.annotations.SerializedName


data class TwitterUser (

    @SerializedName("name")
    var userName: String? = null,


    @SerializedName("profile_background_image_url_https")
    var imageUrl: String? = null



)

