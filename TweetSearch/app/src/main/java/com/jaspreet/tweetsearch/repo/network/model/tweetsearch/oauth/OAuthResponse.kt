package com.jaspreet.tweetsearch.repo.network.model.tweetsearch.oauth

import com.google.gson.annotations.SerializedName

data class OAuthResponse (

    @SerializedName("access_token")
    val accessToken: String
)