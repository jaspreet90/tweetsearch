package com.jaspreet.tweetsearch.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection

abstract class BaseActivity<out VM : BaseViewModel> :AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (hasInjector())
             AndroidInjection.inject(this)
    }

    open fun hasInjector() = true

    abstract fun getViewModel(): VM

}