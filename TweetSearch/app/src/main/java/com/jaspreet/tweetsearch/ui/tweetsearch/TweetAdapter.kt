package com.jaspreet.tweetsearch.ui.tweetsearch

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide
import com.jaspreet.tweetsearch.R
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.Twit

import java.util.ArrayList

class TweetAdapter( internal var context: Context) :
    RecyclerView.Adapter<TweetAdapter.ViewHolder>() {

      var tweets: List<Twit> = mutableListOf()
       set(value) {
           field=value
          notifyDataSetChanged()
       }

    fun clear() {
        tweets = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.tweet_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (user, body) = tweets[position]
        holder.title.text = user!!.userName
        holder.message.text = body
        Glide.with(context)
            .load(user.imageUrl)
            .centerCrop()
            .into(holder.imageView)


    }

    override fun getItemCount(): Int {
        return tweets.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        val message: TextView = itemView.findViewById<View>(R.id.message) as TextView
        val imageView: ImageView = itemView.findViewById<View>(R.id.userImage) as ImageView

    }
}
