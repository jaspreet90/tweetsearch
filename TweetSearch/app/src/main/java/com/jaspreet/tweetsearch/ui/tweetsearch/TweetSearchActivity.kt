package com.jaspreet.tweetsearch.ui.tweetsearch

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.View
import com.jaspreet.tweetsearch.R
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.Twit
import com.jaspreet.tweetsearch.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_tweet_search.*
import javax.inject.Inject



class TweetSearchActivity : BaseActivity<TweetSearchViewModel>() {

    @Inject   lateinit var tweetSearchViewModel:TweetSearchViewModel

    private var mAdapter: TweetAdapter? = null

    override fun getViewModel(): TweetSearchViewModel  = tweetSearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tweet_search)

        setUpUI()
    }

    private fun setUpUI(){
        setUpRecyclerView()
        setUpSearchView()
        bindViewModels()
    }

    private fun setUpRecyclerView(){
        var linearLayoutManager= LinearLayoutManager(this)
        rv_tweet.layoutManager = linearLayoutManager
        mAdapter = TweetAdapter(this)
        rv_tweet.adapter = mAdapter
     }

    fun searchTweets(q:String){
        progress_bar.visibility=View.VISIBLE
        tweetSearchViewModel.search(q)
    }


    fun bindViewModels(){
        tweetSearchViewModel.searchTweetObservable().observe(this, Observer {

            Log.d("==tweet search res=","${it}")
            setData(it)
        })
    }

    fun setData(it: List<Twit>?) {
        progress_bar.visibility=View.GONE

        it?.let { it1 ->  mAdapter?.tweets = it1 }
    }

    private fun setUpSearchView(){

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    unsubscribeTweet()
                    searchTweets(it) }
                return true
             }

            override fun onQueryTextChange(p0: String?) = false
        }
        )
    }

    override fun onBackPressed() {
        super.onBackPressed()

        unsubscribeTweet()
    }

    fun unsubscribeTweet(){
        tweetSearchViewModel.unSubscribeTweet()
    }
}

