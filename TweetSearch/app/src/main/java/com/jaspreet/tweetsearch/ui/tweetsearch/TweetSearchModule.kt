package com.jaspreet.tweetsearch.ui.tweetsearch

import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides

@Module
class TweetSearchModule {

    @Provides
    fun provideViewModel(tweetSearchActivity: TweetSearchActivity , tweetSearchViewModelFactory: TweeterSerachViewModelFactory) = ViewModelProvider(tweetSearchActivity, tweetSearchViewModelFactory).get(TweetSearchViewModel::class.java)
}