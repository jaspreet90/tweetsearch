package com.jaspreet.tweetsearch.ui.tweetsearch

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.gson.Gson
import com.jaspreet.tweetsearch.domain.TweetSearchUseCase
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.TweetsResponse
import com.jaspreet.tweetsearch.repo.network.model.tweetsearch.Twit
import com.jaspreet.tweetsearch.ui.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class TweetSearchViewModel(  val tweetSearchUseCase: TweetSearchUseCase ) : BaseViewModel()  {

       var searchTweeetLiveData =MutableLiveData<List<Twit>>()
    fun searchTweetObservable() = searchTweeetLiveData

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun clearDispose(){

        compositeDisposable?.let {
            if (!it.isDisposed)
                it.dispose()
        }
    }

    fun initCompositeDisposable(){
        compositeDisposable = CompositeDisposable(  )
    }


    fun search(query:String){


            addDisposable(  tweetObservable(query) !!.subscribe({

                  Log.d("==subscribe==",Gson().toJson(it))
                  searchTweeetLiveData.postValue(it.tweets)
              },{
                  Log.e("==error==","${it}")
              })
            )
    }


      var tweetObservable : Observable<TweetsResponse>? = null
    private fun tweetObservable(query: String) : Observable<TweetsResponse>?{

         tweetObservable=  Observable.interval(0,10, TimeUnit.SECONDS, Schedulers.io())
            .flatMap{
                tweetSearchUseCase.searchTweets(query)
            }.observeOn(Schedulers.io())
          .subscribeOn(Schedulers.io())

        return tweetObservable

    }

    fun unSubscribeTweet(){
        clearDispose()
        initCompositeDisposable()

    }

}