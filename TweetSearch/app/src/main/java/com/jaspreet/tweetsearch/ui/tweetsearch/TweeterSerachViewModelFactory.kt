package com.jaspreet.tweetsearch.ui.tweetsearch

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.tweetsearch.domain.TweetSearchUseCase
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TweeterSerachViewModelFactory @Inject constructor(
    private val tweeterSearchUseCase: TweetSearchUseCase
    ) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TweetSearchViewModel::class.java)) {

            return TweetSearchViewModel(tweeterSearchUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}